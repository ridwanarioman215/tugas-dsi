const express = require('express');
const router = express.Router();

const {GetAll,GetId,GetType,edit,store,hapus} = require('../controllers/BooksController');

router
  .get('/',GetAll);
router
  .get('/:id',GetId);
  router
  .get('/jenis/:jenis',GetType);
  router
  .put('/:id',edit);
  router
  .post('/',store);

  router
  .delete('/:id',hapus);

module.exports = router;