const model = require("../models/data")

const GetAll = async(req,res) =>{
    try{
        let dataBuku = await model
        jsonFormat(res,'success',"Success",dataBuku)
    }catch(err){
        jsonFormat(res,'failed',err.message||"gagal",[])
    }
}

const GetId = async(req,res) =>{
    const {id} = req.params
    try{
        let dataBuku = model.find((item)=>item.id === parseInt(id))
        if(!dataBuku){
            let erro = new Error("id buku yang anda cari tidak tersedia")
            erro.statusCode = 400
            throw erro
        }
        jsonFormat(res,'success',"Success",dataBuku)
    }catch(err){
        jsonFormat(res,'failed',err.message||"gagal",[])
    }
}

const GetType = async(req,res) =>{
    try{
        let dataBuku = await model.filter((a)=>a.type === req.params.jenis)
        jsonFormat(res,'success',"Success",dataBuku)
    }catch(err){
        jsonFormat(res,'failed',err.message||"gagal",[])
    }
}

const edit = async(req,res) =>{
    const {id} = req.params
    const {name,type} = req.body
    try{
        if(!name || !type){
            let erro = new Error("inputan body tidak lengkap. harap isi name dan type")
            erro.statusCode = 400
            throw erro
        }
        let dataBuku = model.find((item)=>item.id === parseInt(id))
        if(!dataBuku){
            let erro = new Error("id buku yang anda cari tidak tersedia")
            erro.statusCode = 400
            throw erro
        }
        Object.assign(dataBuku, {name,type});

        let dataBukuSetelahEdit = model.find((item)=>item.id === parseInt(id))
        jsonFormat(res,'success',"Success",dataBukuSetelahEdit)
    }catch(err){
        jsonFormat(res,'failed',err.message||"gagal",[])
    }
}

const store = async(req,res) =>{
    const {name,type} = req.body
    try{
        if(!name || !type){
            let erro = new Error("inputan body tidak lengkap. harap isi name dan type")
            erro.statusCode = 400
            throw erro
        }
        let maxID = model.reduce((max, item) => (item.id > max ? item.id : max), 0);
        let newID = parseInt(maxID)+1
        model.push({
            "id":newID,
            name,
            type
        })
        let dataBuku = model
        jsonFormat(res,'success',"Success",dataBuku)
    }catch(err){
        jsonFormat(res,'failed',err.message||"gagal",err)
    }
}

const hapus = async(req,res)=>{
    const {id} = req.params
    try{
        let cekdata = model.find((item)=>item.id === parseInt(id))
        if(!cekdata){
            let erro = new Error("id yang anda inputkan tidak ada dalam data")
            erro.statusCode = 400
            throw erro
        }
        let dataHapus = model.indexOf(cekdata);
        model.splice(dataHapus, 1);
        let dataBuku = model
        jsonFormat(res,'success',"Success",dataBuku)
    }catch(err){
        jsonFormat(res,'failed',err.message||"gagal",err)
    }
}

const jsonFormat = (res, status, msg, data = []) =>{
    
    if (status === "success") {
        res.json({
          message: msg,
          data: data,
        });
      }
    
      if (status === "failed") {
        res.statusCode = data.statusCode || 401;
        res.json({
          message: msg,
          data: data,
        });
      }

}

module.exports = {GetAll,GetId,GetType,edit,store,hapus}